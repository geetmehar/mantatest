Here is the code flow - 

1. When webapp loads

- App calls the function to getPostList(0)

- First time page loading makes the api call to http://jsonplaceholder.typicode.com/posts?_start=0&_limit=10 by passing '0' as parameter

- Once response is received then the post list is appended to the web page. There is a page index value updated which acts as the page index. This page index value will help us find the next page to load when next button is pressed.

- If error is received the error handling code needs to be present which isn't added by comment is placed in the code.

3. When user clicks on next.

- Page index value is captured and incremented by 1.

- This page index value is now passed to getPostList(pageIndex) and same code flow as in step 1 happens.

- Just like next, a previous button can be placed and again getPostList() can be called to repeat the same code flow as step 1.