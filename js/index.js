console.log('Page loading')

// ***Global variables are defined here

// End of defining global variables


// ***Functions are defined from here

// Function to append post list to webpage
appendPostList = function(postArray, pageIndex) {

	$('#loadingSpinner').css('display', 'none');

	for(i = 0; i < postArray.length ; i++) {

		var post = '<a href="#" class="list-group-item list-group-item-action flex-column align-items-start"><div class="d-flex w-100 justify-content-between"><h5 class="mb-1">' + postArray[i].title + '</h5></div><p class="mb-1">' + postArray[i].body + '</p></a>'

		$('.postList').append(post)
	}

	console.log(parseInt(pageIndex) + 1)

	$('#pageIndex').text(parseInt(pageIndex) + 1)

}
// End of function to append post list to webpage


// Function to make an API call to get the JSON object with list of posts.
getPostList = function(pageIndex){

	$.ajax({
		method: 'GET',
		url: 'https://jsonplaceholder.typicode.com/posts?_start=' + pageIndex + '&_limit=10' ,
		dataType: 'json'
	}).done(function(info){

		console.log(info);

		// Add object received to the web page
		appendPostList(info, pageIndex)

	}).fail(function(info){
								
		console.log(info);

		// Error Handling on API error response

	})

}
// End of making an API call to get the JSON object with list of posts.


// ***End of defining functions




// ***Code to run when webpage loads

// Call the function to get first set of post list when page loads

$('#loadingSpinner').css('display', 'block');

getPostList(0)

// ***End of code to run when webpage loads



// ***Trigger events are defined from here

// When user clicks on next button to load next set of posts
$('html').on('click', '.nextButton', function(){

	console.log($('#pageIndex').text())

	$('.postList').empty()

	$('#loadingSpinner').css('display', 'block');

	getPostList($('#pageIndex').text())

})
// End of when user clicks on next button to load next set of posts

// ***End of defining trigger events